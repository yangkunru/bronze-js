// TODO:
//
// Implement a function called `compact`. This function creates an array with
// all falsey values removed. The values `false`, `null`, `0`, `""`, `undefined`,
// and `NaN` are falsey.
//
// You can refer to unit test for more details.
//
// <--start-
function compact (inputArray) {
  if (inputArray === null || inputArray === undefined) {
    return inputArray;
  }
  const nonFalseyArray = inputArray.reduce((accumulator, value) => {
    if (value) {
      accumulator.push(value);
    }
    return accumulator;
  }, []);
  return nonFalseyArray;
}
// --end-->

export default compact;
