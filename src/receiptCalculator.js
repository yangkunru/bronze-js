class RecepitCalculator {
  constructor (products) {
    this.products = [...products];
  }

  // TODO:
  //
  // Implement a method to calculate the total price. Please refer to unit test
  // to understand the requirement.
  //
  // Note that you can only modify code within the specified area.
  // <-start-
  getTotalPrice (enquiryArray) {
    const totalPrice = enquiryArray.reduce((sum, id) => {
      const product = this.products.find(product => product.id === id);
      if (product === undefined) {
        throw new Error('Product not found.');
      }
      sum += product.price;
      return sum;
    }, 0);
    return totalPrice;
  }
  // --end->
}

export default RecepitCalculator;
