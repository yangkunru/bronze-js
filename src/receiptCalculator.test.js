import ReceiptCalculator from './receiptCalculator';

describe('when calculating receipt', () => {
  const products = [
    { id: 'BX2188', name: 'Coca Cola', price: 3 },
    { id: 'BX2192', name: 'Dr. Pepper', price: 4 },
    { id: 'BX3333', name: 'Pepsi Cola', price: 2 }
  ];

  fit('should calculate sum of product prices', () => {
    const calculator = new ReceiptCalculator(products);
    const totalPrice = calculator.getTotalPrice(['BX2188', 'BX2192', 'BX3333']);
    expect(totalPrice).toEqual(9);
  });

  fit('should calculate sum of selected product prices', () => {
    const calculator = new ReceiptCalculator(products);
    const totalPrice = calculator.getTotalPrice(['BX3333', 'BX2188']);
    expect(totalPrice).toEqual(5);
  });

  fit('should return 0 if no product is selected', () => {
    const calculator = new ReceiptCalculator(products);
    const totalPrice = calculator.getTotalPrice([]);
    expect(totalPrice).toEqual(0);
  });

  fit('should throw if product is not found', () => {
    const calculator = new ReceiptCalculator(products);
    expect(() => calculator.getTotalPrice(['What the hell is this?'])).toThrow('Product not found.');
  });
});
